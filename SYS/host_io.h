#ifndef SYS_HOST_IO_H
#define SYS_HOST_IO_H

#include <SDR/BASE/common.h>
#include <SDR/BASE/Abstract.h>

#include "host_io_base/target.h"
#include "host_io_base/host.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  void * Master;
} HOST_IO_Cfg_t;

typedef struct
{
  HOST_IO_Cfg_t cfg;
} HOST_IO_t;

void init_HOST_IO(HOST_IO_t * This, HOST_IO_Cfg_t * Cfg);

void HOST_IO_setTargetState(HOST_IO_t * This, TARGET_State_t state);
TARGET_State_t HOST_IO_getState(HOST_IO_t * This);

Bool_t HOST_IO_isStarted(HOST_IO_t * This);
Bool_t HOST_IO_isRunning(HOST_IO_t * This);

Bool_t HOST_IO_send_uint8(HOST_IO_t * This, TARGET_DataId_t id, UInt8_t data, Size_t bitsCount);
Bool_t HOST_IO_send_data(HOST_IO_t * This, TARGET_DataId_t id, UInt8_t * Buf, Size_t Count);

Bool_t HOST_IO_send_response(HOST_IO_t * This, TARGET_Response_t Resp);
Bool_t HOST_IO_send_state_response(HOST_IO_t * This, TARGET_State_t State);

Bool_t HOST_IO_send_params(HOST_IO_t * This, TARGET_Params_t * params);
Bool_t HOST_IO_send_name(HOST_IO_t * This, const char * name);

Bool_t HOST_IO_send_format(HOST_IO_t * This, TARGET_DataId_t id, const char * format);

Bool_t HOST_IO_send_element_value(HOST_IO_t * This, TARGET_ElementType_t type, TARGET_DataId_t id, const char * val);
Bool_t HOST_IO_send_element_params(HOST_IO_t * This, TARGET_ElementType_t type, TARGET_DataId_t id, const char * params);
Bool_t HOST_IO_send_plot_command(HOST_IO_t * This, TARGET_DataId_t plotId, TARGET_PlotCommand_t cmd);
Bool_t HOST_IO_send_plot_command_ext(HOST_IO_t * This, TARGET_DataId_t plotId, TARGET_PlotCommand_t cmd, const char * ext);

Bool_t HOST_IO_send_samples(HOST_IO_t * This, TARGET_DataId_t id, Sample_t * Buf, Size_t Count);
Bool_t HOST_IO_send_samples_iq(HOST_IO_t * This, TARGET_DataId_t id, iqSample_t * Buf, Size_t Count);

Bool_t HOST_IO_send_log_message(HOST_IO_t * This, const char * message);

void HOST_IO_startPaused(HOST_IO_t * This);

void HOST_IO_continue(HOST_IO_t * This);
void HOST_IO_start(HOST_IO_t * This);
void HOST_IO_pause(HOST_IO_t * This);
void HOST_IO_step(HOST_IO_t * This);
void HOST_IO_reset(HOST_IO_t * This);
void HOST_IO_stop(HOST_IO_t * This);

#ifdef __cplusplus
}
#endif

#endif // SYS_HOST_IO_H
