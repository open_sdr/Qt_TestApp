#ifndef TESTCONTROL_H
#define TESTCONTROL_H

#include <QWidget>
#include <QShortcut>

#include "AbstractTestApp.h"
#include "TestLogs.h"
#include <SDR/Qt_Addons/Controls/TPlotsWidget.h>

namespace Ui {
class TestControl;
}

namespace SDR
{

class TestControl : public QWidget
{
  Q_OBJECT

public:
  explicit TestControl(AbstractTestApp *app, int timeout_ms, QString name, QWidget *parent = 0);
  ~TestControl();

  void setName(QString name);

  void appendUserWidget(QWidget* Widget);
  void deleteUserWidget(QWidget* Widget);

  void appendUserStatusWidget(QWidget* Widget);
  void deleteUserStatusWidget(QWidget* Widget);

  void setPlots(TPlotsWidget * pw);
  TPlotsWidget* Plots();

  TestLogs * Logs(){return &wlogs;}

  void ui_set_exec_timeout(int timeout_ms);
  void ui_set_exec_controls_enabled(bool flag);
  void ui_set_exec_state(AbstractTestApp::ExecState state);
  void ui_set_plots_enabled(bool flag);

  bool ui_ExecControlsEnabled();

signals:
  void closed();

private slots:
  void on_Timeout_editingFinished();
  void on_buttonRun_clicked();
  void on_ButtonStop_clicked();
  void on_buttonSingleStep_clicked();

  void on_buttonHZoom_toggled(bool checked);
  void on_buttonVZoom_toggled(bool checked);
  void on_buttonHDrag_toggled(bool checked);
  void on_buttonVDrag_toggled(bool checked);

  void on_ButtonPause_clicked();

  void on_buttonReset_clicked();

  void on_buttonLogs_clicked();

private:
  Ui::TestControl * ui;
  AbstractTestApp * App;

  TestLogs wlogs;

  enum Action {actStart, actStep, actPause, actStop, ACTIONS_COUNT, actNone = -1};
  QList<QShortcut*> ShortCuts;

  QList<QKeySequence> defaultShortCutsKeys();
  void connectAction(Action act);
  void disconnectAction(Action act);
  void initShortCuts();
  void destroyShortCuts();

  void closeEvent(QCloseEvent *event);
};

}

#endif // TESTCONTROL_H
